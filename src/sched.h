/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de funciones del scheduler
*/

#ifndef __SCHED_H__
#define __SCHED_H__

#include "stdint.h"
#include "screen.h"
#include "tss.h"

typedef struct sched_entry {
	uint8_t indice_gdt; // indice tarea actual
	uint8_t activo; // 1 vivo=activo, 0 muerto=desactivo
} __attribute__((__packed__))  sched_entry;

void sched_init();

int16_t sched_nextTask();

void sched_deactivateCurrentTask();

#endif	/* !__SCHED_H__ */
