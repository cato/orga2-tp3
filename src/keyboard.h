#include "stdint.h"

// https://wiki.osdev.org/%228042%22_PS/2_Controller
// pero sobre todo:
// https://wiki.osdev.org/PS/2_Keyboard
// scan code set 1
#define KB_Y_KEY_DOWN 0x15
#define KB_NUMPAD_RELEASED_BEG 0x82 // el primero es el 1
#define KB_NUMPAD_RELEASED_END 0x8C // el último el 0 en 0x8B

#define KB_W_KEY_DOWN 0x11
#define KB_S_KEY_DOWN 0x1F

#define KB_I_KEY_DOWN 0x17
#define KB_K_KEY_DOWN 0x25

#define KB_L_SHIFT_DOWN 0xAA
#define KB_R_SHIFT_DOWN 0xB6

void isr_kb_c(uint8_t);
