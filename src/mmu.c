/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
   definicion de funciones del manejador de memoria
*/

#include "mmu.h"

uint32_t glb_current_free_kernel_page;
void mmu_init() {
  glb_current_free_kernel_page = KERNEL_FREE_AREA_BEG;
}

uint32_t mmu_nextFreeKernelPage() {
  uint32_t ret = glb_current_free_kernel_page;
  glb_current_free_kernel_page += PAGE_SIZE;
  return ret;
}

void mmu_mapPage_2(uint32_t virtual, uint32_t cr3, uint32_t phy, uint8_t user, uint8_t rw) {
  uint32_t attrs = 0;
  PDE_SET_USER(attrs, user);
  PDE_SET_READ_WRITE(attrs, rw);
  PDE_SET_PRESENT(attrs, 1);
  mmu_mapPage(virtual, cr3, phy, attrs);
}

void mmu_mapPage(uint32_t virtual, uint32_t cr3, uint32_t phy, uint32_t attrs) {

  pdt_entry *pdt_entry_pt = (pdt_entry *)cr3;
  uint32_t pd_index = PDE_INDEX(virtual);
  if (!PTE_IS_PRESENT(pdt_entry_pt[pd_index])) {
    pdt_entry_pt[pd_index] = mmu_nextFreeKernelPage(); // nos va a dar algo alineado a 4K
    pt_entry *pt_entry_pt = (pt_entry *)pdt_entry_pt[pd_index];
    for (uint32_t i = 0; i < 1024; i++)
      pt_entry_pt[i] = 0;
    pdt_entry_pt[pd_index] |= 7;
  }

  pt_entry* pt_entry_pt = (pt_entry *)(pdt_entry_pt[pd_index] & PHY_MASK);
  uint32_t pt_index = PTE_INDEX(virtual);
  pt_entry_pt[pt_index] = phy & PHY_MASK;
  pt_entry_pt[pt_index] |= attrs;
  PTE_SET_PRESENT(pt_entry_pt[pt_index], 1);

  /* metodo de fuerza bruta para actualizar cache de traducciones*/
  tlbflush();
}

pt_entry* mmu_unmapPage(uint32_t virtual, uint32_t cr3) {

  pdt_entry *pdt_entry_pt = (pdt_entry *)cr3;
  uint32_t pd_index = PDE_INDEX(virtual);

  pt_entry *pt_entry_pt = (pt_entry *)(pdt_entry_pt[pd_index] & PHY_MASK);
  uint32_t pt_index = PTE_INDEX(virtual);
  PTE_SET_PRESENT(pt_entry_pt[pt_index], 0);

  /* metodo de fuerza bruta para actualizar cache de traducciones*/
  tlbflush();

  return &pt_entry_pt[pt_index];
}

uint32_t mmu_initKernelDir() {
  pdt_entry *pdt_entry_pt = (pdt_entry*) KERNEL_PAGE_DIR;

  for (uint32_t i = 0; i < 1024; i++)
    pdt_entry_pt[i] = 0;

  *pdt_entry_pt = KERNEL_PAGE_TABLE_0; /* primer entrada a la primer page table */
  PDE_SET_PRESENT(*pdt_entry_pt, 1);
  PDE_SET_USER(*pdt_entry_pt, 0);
  PDE_SET_READ_WRITE(*pdt_entry_pt, 1);

  pt_entry *pt_entry_pt = (pt_entry*) KERNEL_PAGE_TABLE_0;
  for (uint32_t i = 0; i < 1024; i++, pt_entry_pt++) {
    *pt_entry_pt = i << 12;
    PTE_SET_PRESENT(*pt_entry_pt, 1);
    PTE_SET_USER(*pt_entry_pt, 0);
    PTE_SET_READ_WRITE(*pt_entry_pt, 1);
  }

  tlbflush();
  return KERNEL_PAGE_DIR;
}

uint32_t mmu_initTaskDir(uint32_t virt, uint32_t phys,
                         uint32_t shared, uint32_t task) {

  uint32_t pd_addr = mmu_nextFreeKernelPage();

  /* inicializo PD con 0s */
  uint32_t *pt = (uint32_t *)pd_addr;
  for (uint32_t i = 0; i < 1024; i++) {
    pt[i] = 0;
  }
  uint32_t attr = 0;
  PTE_SET_PRESENT(attr, 1);
  PTE_SET_READ_WRITE(attr, 0);
  PTE_SET_USER(attr, 1);
  /* identity mapping, solo lectura */
  for (uint32_t i = 0; i < 1024; i++) {
    uint32_t addr = i << 12;
    mmu_mapPage(addr, pd_addr, addr, attr);
  }

  /* mapeamos area compartida */
  PTE_SET_READ_WRITE(attr, 1);
  mmu_mapPage(virt + 2 * PAGE_SIZE, pd_addr, shared, attr);
  mmu_mapPage(virt + 3 * PAGE_SIZE, pd_addr, shared + PAGE_SIZE, attr);
  return pd_addr;
}

void mmu_reMapTask(uint32_t new_phys, uint32_t cr3, uint32_t old_phys) {
  mmu_copyPhysical(new_phys, old_phys); // copio 2 paginas
  mmu_copyPhysical(new_phys + PAGE_SIZE, old_phys+PAGE_SIZE);
  /* mapeamos código y pila */
  mmu_mapPage_2(TASK_CODE, cr3, new_phys, 1, 1); // codigo, datos y pila
  mmu_mapPage_2(TASK_CODE + PAGE_SIZE, cr3, new_phys + PAGE_SIZE, 1, 1);
}

void mmu_copyPhysical(uint32_t phy_dst, uint32_t phy_src) {
  // Tenemos que copiar memoria, pero tenemos activada paginación.
  // Hacemos los mapeos usando el crt3 actual, con dos virtuales
  // dentro del id mapping (así es facil de remapear luego) que
  // sabemos que no apuntan a nada util.
  uint32_t vrt_src = 0x2B000;
  uint32_t vrt_dst = 0x2C000;
  // nos quedamos con los permisos
  pt_entry src_perms = *mmu_unmapPage(vrt_src, rcr3()) & (~PHY_MASK);
  pt_entry dst_perms = *mmu_unmapPage(vrt_dst, rcr3()) & (~PHY_MASK);
  mmu_mapPage(vrt_src, rcr3(), phy_src, 0x3);
  mmu_mapPage(vrt_dst, rcr3(), phy_dst, 0x3);
  for (int i = 0; i < 1024; i++) {
    ((uint32_t *)vrt_dst)[i] = ((uint32_t *)vrt_src)[i];
  }
  // recupero el identity mapping
  mmu_mapPage(vrt_src, rcr3(), vrt_src, src_perms);
  mmu_mapPage(vrt_dst, rcr3(), vrt_dst, dst_perms);
}
