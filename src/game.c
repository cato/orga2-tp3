/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
*/

#include "game.h"
#include "defines.h"

extern uint8_t tareaActualAB[];
extern unsigned int current_player;
extern sched_entry tareasA[10];
extern sched_entry tareasB[10];
extern uint8_t cantidad_tareasAB[2];

extern uint32_t tasksA_cr3[10];
extern uint32_t tasksB_cr3[10];

extern uint8_t game_screen_echo_cursor;
extern screen_sty_t screen_sty;

player_t playerA;
player_t playerB;

//uint32_t** map = (uint32_t **)CRYSTALS_MAP;
uint32_t (*map)[BOARD_SIZE_X] = (uint32_t (*)[BOARD_SIZE_X]) CRYSTALS_MAP;

player_t * players[2] = {&playerA, &playerB};

void game_init() {
  playerA.pos.x = 0;
  playerA.pos.y = BOARD_SIZE_Y / 2;
  playerB.pos.x = VIDEO_COLS-1;
  playerB.pos.y = playerA.pos.y;

  playerA.id = 0;
  playerA.nminers = &cantidad_tareasAB[0];
  playerB.id = 1;
  playerB.nminers = &cantidad_tareasAB[1];

  for (int i = 0; i < 10; i++) {
    playerA.miners[i].bag = 0;
    playerA.miners[i].active = &tareasA[i].activo;
    playerA.miners[i].cr3 = &tasksA_cr3[i];
  }
  for (int i = 0; i < 10; i++) {
    playerB.miners[i].bag = 0;
    playerB.miners[i].active = &tareasB[i].activo;
    playerB.miners[i].cr3 = &tasksB_cr3[i];
  }
  /* inicializamos los cristales en forma de rombos concéntricos */
  uint8_t center_x = BOARD_SIZE_X / 2;
  uint8_t center_y = BOARD_SIZE_Y / 2;
  for (uint32_t y = 0; y < BOARD_SIZE_Y; y++) {
    for (uint32_t x = 0; x < BOARD_SIZE_X; x++){
      uint32_t crystals = 0;
      uint32_t dist_center_x = center_x > x ? center_x - x : x - center_x;
      uint32_t dist_center_y = center_y > y ? center_y - y : y - center_y;
      uint32_t dist_center = dist_center_x + dist_center_y;
      if(dist_center < 8){
        crystals = 4;
      }if(dist_center >= 8){
        crystals = 2;
      }if(dist_center > 16){
        crystals = 1;
      }if(dist_center > 32){
        crystals = 0;
      }
      map[x][y] = crystals;
    }
  }
  
  game_screen_init();
}

void game_newTask(player_t* player, e_direction_t dir){
  if(*player->nminers >= 10)
    return;

  uint8_t iminer = *player->nminers;
  miner_t miner = player->miners[iminer];
  while (*miner.active) {
    iminer = (iminer + 1) % 10;
    miner = player->miners[iminer];
  }

  miner.pos = player->pos;

  if (dir == Left) // La posicion X del player de la izquierda es 0, y esta bien
    miner.pos.x = miner.pos.x - 2;  //La posicion X del player de la derecha es 79, debe ser 77
  *miner.active = 1;

  miner.pos.y--;
  player->miners[iminer] = miner;
  (*player->nminers)++;

  uint32_t new_phys = GAME_POS_TO_PHY_ADDR(miner.pos.x, miner.pos.y);
  mmu_reMapTask(new_phys, *(miner.cr3),
                player->id == 0 ? TASK_A_CODE : TASK_B_CODE);
  game_screen_minerPut(miner.sty, miner.pos);
}

uint8_t game_getId() {
  uint8_t cp = current_player;
  return tareaActualAB[cp];
}

void game_movePlayer(player_t *player, e_direction_t dir) {
  pos_t old_pos = player->pos;
  if (dir == Left){
    if (!player->id)
      player->pos.y--;
    else
      player->pos.y++;
  }else{
    if (!player->id)
      player->pos.y++;
    else
      player->pos.y--;
  }
  if (player->pos.y > BOARD_SIZE_Y) // si bien -1 % 5 = 4, por algún motivo (representación?) no anda
    player->pos.y = player->pos.y - BOARD_SIZE_Y;
  if (player->pos.y <= 0)
    player->pos.y = BOARD_SIZE_Y + player->pos.y;

  game_screen_echo("pos.y:");
  game_screen_echoDec(player->pos.y);
  game_screen_echo("pos.x:");
  game_screen_echoDec(player->pos.x);
  game_screen_echo_cursor = 0;
  game_screen_playerRefresh(player->sty, player->pos, old_pos);
}

void game_killMiner(uint8_t player_id, uint8_t task_id) {
  player_t* pl = players[player_id];
  miner_t miner = pl->miners[task_id];
  pl->nminers--;
  *miner.active = 0;
  screen_putCharWithAttr(screen_sty.board.cha, screen_sty.board.col, miner.pos.x + 1, miner.pos.y + 1);
  game_screen_crystalPut(miner.pos.x, miner.pos.y);
}

/* Servicios */

void game_moveTask(e_direction_t direction){
  uint8_t cp = current_player;
  uint8_t ct = tareaActualAB[cp];
  miner_t* miner = &players[cp]->miners[ct];
  pos_t old_pos = miner->pos;
  uint32_t *the_bag = &players[cp]->miners[ct].bag;

  int8_t step = 1;
  if (cp) // si es el de la derecha, invertir movimientos
    step = -1;

  switch (direction) {
  case Right:
    miner->pos.y = (miner->pos.y + step) % BOARD_SIZE_Y;
    break;
  case Left:
    miner->pos.y = (miner->pos.y - step) % BOARD_SIZE_Y;
    break;
  case Forward:
    miner->pos.x = (miner->pos.x + step) % BOARD_SIZE_X;
    break;
  case Back:
    miner->pos.x = (miner->pos.x - step) % BOARD_SIZE_X;
    break;
  }

  if (miner->pos.y < 0)
    miner->pos.y = BOARD_SIZE_Y -1; // por algún motivo turbio^n % no funca bien con x<0

  if (miner->pos.x >= BOARD_SIZE_X - 1) {
    miner->pos.x = BOARD_SIZE_X - 1;
    players[1]->score += *the_bag;
    *the_bag = 0;
  }

  if (miner->pos.x <= 0) {
    miner->pos.x = 0;
    players[0]->score += *the_bag;
    *the_bag = 0;
  }

  // to video coords
  uint32_t old_phys = GAME_POS_TO_PHY_ADDR(old_pos.x, old_pos.y);
  uint32_t new_phys = GAME_POS_TO_PHY_ADDR(miner->pos.x, miner->pos.y);
  mmu_reMapTask(new_phys, rcr3(), old_phys);
  game_screen_minerRefresh(miner->sty, miner->pos, old_pos);
}

uint32_t game_take() {
  uint32_t bag = 0;
  uint8_t cp = current_player;
  uint8_t ct = tareaActualAB[cp];
  pos_t tsk_pos;
  tsk_pos = players[cp]->miners[ct].pos;
  bag = players[cp]->miners[ct].bag;

  uint32_t crystals = ((uint32_t **)CRYSTALS_MAP)[tsk_pos.x][tsk_pos.y];
  if (crystals <= 0) return -1;
  if (crystals + bag > 30)
    return 0;

  return crystals;
}
