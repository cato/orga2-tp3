/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  rutina/s para debuggear
*/

#ifndef __GAME_SCREEN__
#define __GAME_SCREEN__

#include "stdint.h"
#include "geom.h"

typedef struct sty_t {
  uint8_t col;  // color foreground | background
  uint8_t cha;  // símbolo / caracter
} sty_t;

typedef struct str_dbg_box_sty {
  sty_t banner;
  sty_t borders;
  sty_t body;
  sty_t regs;
  sty_t vals;
  pos_t ini;
  pos_t end;
} dbg_box_sty_t;

typedef struct str_screen_sty {
  sty_t playerA_corridor;
  sty_t playerB_corridor;
  sty_t board;
} screen_sty_t;

typedef struct str_player_sty {
  sty_t sty;
  sty_t* corridor;
} player_sty_t;

void game_screen_init();

void game_screen_printDebugBox(uint8_t exc_n, uint32_t *data);
void game_screen_drawGameBoard();

void game_screen_minerPut(sty_t miner_sty, pos_t pos);
void game_screen_playerPut(sty_t player_sty, pos_t pos);
void game_screen_crystalPut(uint8_t x, uint8_t y);

void game_screen_playerRefresh(player_sty_t player_sty,
                                              pos_t new_pos, pos_t old_pos);
void game_screen_minerRefresh(sty_t miner_sty, pos_t new_pos, pos_t old_pos);

/* otras */
void game_screen_echo(char *msg);
void game_screen_echoDec(uint8_t n);
void game_screen_echoClean();

#endif
