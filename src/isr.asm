; ** por compatibilidad se omiten tildes **
; ==============================================================================
; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
; ==============================================================================
; definicion de rutinas de atencion de interrupciones

%include "print.mac"

BITS 32

sched_task_offset:     dd 0x00
sched_task_selector:   dw 0x00

global debug_mode_on
debug_mode_on: db 0x00

;; PIC
extern pic_finish1

;; Sched
extern sched_nextTask
extern sched_deactivateCurrentTask

;; Game
extern game_moveTask
extern game_take
extern game_screen_printDebugBox
extern game_getId

;; interrupts
extern keyboard
;;
;; Definición de MACROS
;; -------------------------------------------------------------------------- ;;

%macro ISR 1
global _isr%1

_isr%1:
    pushad
    call pic_finish1
    cmp byte [debug_mode_on], 0
    je .desalojar_tarea
    push esp
    push %1
    call game_screen_printDebugBox
    add esp, 8
    jmp $                               ;TODO: esto se tiene que quedar loopeando hasta apretar Y
    popad
    iret
.desalojar_tarea:
    call sched_deactivateCurrentTask    ; super vagos
    jmp __clock_nextTask                ; vamos al reloj, saltamos a la siguiente tarea y salimos por ahí
%endmacro


;; Rutina de atención de las EXCEPCIONES
;; -------------------------------------------------------------------------- ;;
ISR 0
ISR 1
ISR 2
ISR 3
ISR 4
ISR 5
ISR 6
ISR 7
ISR 8
ISR 9
ISR 10
ISR 11
ISR 12
ISR 13
ISR 14
ISR 15
ISR 16
ISR 17
ISR 18
ISR 19

;; Rutina de atención del RELOJ
;; -------------------------------------------------------------------------- ;;

global _isr32
_isr32:
    pushad
    call pic_finish1
    call nextClock
__clock_nextTask:
    call sched_nextTask

    str cx
    cmp ax, cx
    je .fin

    mov [sched_task_selector], ax
    jmp far [sched_task_offset]

    .fin:
    popad
    iret

;; Rutina de atención del TECLADO
;; -------------------------------------------------------------------------- ;;

global _isr33
_isr33:
    pushad
    xor eax, eax
    in al, 0x60                         ; tomo valor del teclado
    push eax
    call keyboard                       ; mejor desde C...
    add esp, 4                          ; restauro la pila
    call pic_finish1
    popad
    iret

;; Rutinas de atención de las SYSCALLS
;; -------------------------------------------------------------------------- ;;

service_return_val: dd 0x0

global _isr47
_isr47:
    pushad
    cmp eax, 177788
    je .move
    cmp eax, 310311
    je .take
    cmp eax, 760279
    je .getId
    xchg bx, bx
    ; si eax no tiene ninguno de los servicios, la tarea debe percibir pena capital.  por ahora intentamos
    ; escribir en kernel 0xCACA usando el selector de código, para encabronar al cpu y que se arroje GP
    mov word [cs:0], 0xCACA

.move:
    push ebx
    call game_moveTask
    add esp, 4
    jmp .fine
.take:
    call game_take
    jmp .fine
.getId:
    call game_getId

.fine:
    mov dword [service_return_val], eax
    call pic_finish1                    ; TODO: poner al principio preservando eax
    popad
    mov eax, [service_return_val]
    iret

;; Funciones Auxiliares
;; -------------------------------------------------------------------------- ;;
isrNumber:           dd 0x00000000
isrClock:            db '-\|/'
nextClock:
        pushad
        inc DWORD [isrNumber]
        mov ebx, [isrNumber]
        cmp ebx, 0x4
        jl .ok
                mov DWORD [isrNumber], 0x0
                mov ebx, 0
        .ok:
                add ebx, isrClock
                print_text_pm ebx, 1, 0x0f, 49, 79
                popad
        ret
