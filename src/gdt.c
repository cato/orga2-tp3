/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de la tabla de descriptores globales
*/

#include "gdt.h"

gdt_entry gdt[GDT_COUNT] = {
    /* Descriptor nulo*/
    /* Offset = 0x00 */
    [GDT_IDX_NULL_DESC] = (gdt_entry) {
        (uint16_t)    0x0000,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x00,           /* type         */
        (uint8_t)     0x00,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x00,           /* p            */
        (uint8_t)     0x0F,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x00,           /* db           */
        (uint8_t)     0x00,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    /* pag 42 diapo practica */
    /* Estos segmentos deben direccionar los primeros 66MB de memoria.
       Como direcciono más de 1MB necesito granularidad 1.
       El direccionamiento a 4K es 0x4200 y en la gdt se le resta 1*/
    [GDT_IDX_CODE_LVL_0] = (gdt_entry) {
        (uint16_t)    0xFFFF,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x08,           /* type         */ /* execute only */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x0F,           /* limit[16:19] */
        (uint8_t)     0x01,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    [GDT_IDX_DATA_LVL_0] = (gdt_entry) {
        (uint16_t)    0xFFFF,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x02,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x0F,           /* limit[16:19] */
        (uint8_t)     0x01,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    [GDT_IDX_CODE_LVL_3] = (gdt_entry) {
        (uint16_t)    0xFFFF,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x0B,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x03,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x0F,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    [GDT_IDX_DATA_LVL_3] = (gdt_entry) {
        (uint16_t)    0xFFFF,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x02,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x03,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x0F,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    [GDT_IDX_VIDEO_LVL_0] = (gdt_entry) {
        (uint16_t)    0x011F,         /* limit[0:15]  */
        (uint16_t)    0x8000,         /* base[0:15]   */
        (uint8_t)     0x0b,           /* base[23:16]  */
        (uint8_t)     0x02,           /* type         */
        (uint8_t)     0x01,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x01,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x01,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x01,           /* db           */
        (uint8_t)     0x01,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
	 /* TSS procesos */
    [GDT_TSS_INITIAL] = (gdt_entry) {
        (uint16_t)    0x0000,         /* limit[0:15]  */
        (uint16_t)    0x0000,         /* base[0:15]   */
        (uint8_t)     0x00,           /* base[23:16]  */
        (uint8_t)     0x00,           /* type         */
        (uint8_t)     0x00,           /* s            */
        (uint8_t)     0x00,           /* dpl          */
        (uint8_t)     0x00,           /* p            */
        (uint8_t)     0x00,           /* limit[16:19] */
        (uint8_t)     0x00,           /* avl          */
        (uint8_t)     0x00,           /* l            */
        (uint8_t)     0x00,           /* db           */
        (uint8_t)     0x00,           /* g            */
        (uint8_t)     0x00,           /* base[31:24]  */
    },
    [GDT_TSS_IDLE] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A0] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A1] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A2] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A3] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A4] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A5] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A6] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A7] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A8] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_A9] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B0] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B1] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B2] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B3] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B4] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B5] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B6] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B7] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B8] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    },
    [GDT_TSS_TASK_B9] = (gdt_entry) {
        (uint16_t)    0x0067,           /* limit[0:15]  */
        (uint16_t)    0x0000,           /* base[0:15]   */
        (uint8_t)     0x00,             /* base[23:16]  */
        (uint8_t)     0x09,             /* type         */
        (uint8_t)     0x00,             /* s            */
        (uint8_t)     0x00,             /* dpl          */
        (uint8_t)     0x00,             /* p            */
        (uint8_t)     0x00,             /* limit[16:19] */
        (uint8_t)     0x01,             /* avl          */
        (uint8_t)     0x00,             /* l            */
        (uint8_t)     0x00,             /* db           */
        (uint8_t)     0x00,             /* g            */
        (uint8_t)     0x00,             /* base[31:24]  */
    }
};

gdt_descriptor GDT_DESC = {
    sizeof(gdt) - 1,
    (uint32_t) &gdt
};
