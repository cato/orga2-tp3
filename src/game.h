/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
*/

#ifndef __GAME_H__
#define __GAME_H__

#include "stdint.h"
#include "defines.h"
#include "screen.h"
#include "mmu.h"
#include "sched.h"

#include "geom.h"

#include "game_screen.h"

#define GAME_POS_TO_PHY_ADDR(x, y)  (0x400000 + ((x + y*BOARD_SIZE_X))*2*PAGE_SIZE)

typedef enum e_direction {
                          Right = 1,
                          Left = 2,
                          Forward = 3,
                          Back = 4
} e_direction_t;

typedef struct miner {
  pos_t pos;
  uint32_t bag;
  uint8_t * active; // puntero a sched_entry activo
  sty_t sty;
  uint32_t * cr3; // FEO
} miner_t;

typedef struct str_player {
  uint32_t score;
  pos_t pos;
  uint8_t* nminers;
  miner_t miners[10];
  char* name;
  player_sty_t sty;
  uint8_t id;
} player_t;

void game_init();

void game_newTask(player_t *, e_direction_t dir);
void game_movePlayer(player_t *player, e_direction_t dir);
void game_killMiner(uint8_t player_id, uint8_t task_id);

/* Servicios, getId está implementado directamente en el asm */
uint8_t game_getId();
uint32_t game_take();
void game_moveTask(e_direction_t);

#endif  /* !__GAME_H__ */
