/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
   definicion de funciones del manejador de memoria
*/

#ifndef __MMU_H__
#define __MMU_H__

#include "stdint.h"
#include "defines.h"
#include "i386.h"
#include "tss.h"
#include "game.h"

#define KERNEL_FREE_AREA_BEG 0x100000
#define KERNEL_FREE_AREA_END 0x3FFFFF

#define PAGE_SIZE 0x1000

#define PE_INDEX_MASK        0xFFC00000 /* 10 bits mas significativos */
#define PHY_MASK             0xFFFFF000 /* 20 bits mas significativos */

#define CLEAN_USER_BIT_MASK      0xFFFFFFFB
#define CLEAN_RW_BIT_MASK        0xFFFFFFFD
#define CLEAN_PRESENT_BIT_MASK   0xFFFFFFFE

/* dada una virtual devuelve el indice en la PD */
#define PDE_INDEX(virtual) (virtual >> 22) /* hacia menos significativo */
#define PDE_SET_PRESENT(entry, present_bit) ( entry = ( present_bit | ( entry & CLEAN_PRESENT_BIT_MASK ) ) )
#define PDE_SET_USER(entry, usr_bit) ( entry = ( (usr_bit << 2) | ( entry & CLEAN_USER_BIT_MASK ) ) )
#define PDE_SET_READ_WRITE(entry, rd_bit) ( entry = ( (rd_bit  << 1) | ( entry & CLEAN_RW_BIT_MASK ) ) )

typedef uint32_t pdt_entry;
typedef uint32_t pt_entry;

/* Las entradas de la page table comparten los offsets de la PDT, menos el indice */

/* dada una virtual devuelve el indice en la PT */
#define PTE_INDEX(virtual) ( (virtual >> 12) & 0x3FF )
#define PTE_SET_PRESENT(entry, present_bit) PDE_SET_PRESENT(entry, present_bit)
#define PTE_SET_USER(entry, usr_bit) PDE_SET_USER(entry, usr_bit)
#define PTE_SET_READ_WRITE(entry, rd_bit) PDE_SET_READ_WRITE(entry, rd_bit)

#define PTE_IS_PRESENT(entry) ( 0x1 & entry )

void mmu_init();

uint32_t mmu_nextFreeKernelPage();

void mmu_mapPage(uint32_t virtual, uint32_t cr3, uint32_t phy,
                 uint32_t attrs);

void mmu_mapPage_2(uint32_t virtual, uint32_t cr3, uint32_t phy, uint8_t user, uint8_t rw);

/* devuelve un pt_entry, util para recuperar física y permisos a nivel de page table */
pt_entry* mmu_unmapPage(uint32_t virtual, uint32_t cr3);

uint32_t mmu_initKernelDir();

void mmu_reMapTask(uint32_t new_phys, uint32_t cr3, uint32_t old_phys);

/* inicializa PD para virt, mapea shared según especificación.*/
uint32_t mmu_initTaskDir(uint32_t virt, uint32_t phys, uint32_t shared,  uint32_t task);

/* Copia */
void mmu_copyPhysical(uint32_t phys_dst, uint32_t phy_src);

#endif	/* !__MMU_H__ */




