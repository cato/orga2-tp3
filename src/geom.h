#ifndef __GEOM__
#define __GEOM__

#include "stdint.h"

typedef struct str_pos {
  int8_t x, y;
} __attribute__((__packed__)) pos_t; // me parece que packed no hace falta acá

#endif
