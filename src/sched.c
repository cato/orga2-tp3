/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
   definicion de funciones del scheduler
*/
#include "game_screen.h"
#include "sched.h"
#include "tss.h"
uint8_t current_player = 0;
uint8_t cantidad_tareasAB[2] = {0, 0};
uint8_t tareaActualAB[2] = {0, 0};

uint32_t tasksA_cr3[10];
uint32_t tasksB_cr3[10];

sched_entry tareasA[10] = {
						   (sched_entry){GDT_TSS_TASK_A0, 0}
						   , (sched_entry){GDT_TSS_TASK_A1, 0}
						   , (sched_entry){GDT_TSS_TASK_A2, 0}
						   , (sched_entry){GDT_TSS_TASK_A3, 0}
						   , (sched_entry){GDT_TSS_TASK_A4, 0}
						   , (sched_entry){GDT_TSS_TASK_A5, 0}
						   , (sched_entry){GDT_TSS_TASK_A6, 0}
						   , (sched_entry){GDT_TSS_TASK_A7, 0}
						   , (sched_entry){GDT_TSS_TASK_A8, 0}
						   , (sched_entry){GDT_TSS_TASK_A9, 0}};

sched_entry tareasB[10] = {
						   (sched_entry){GDT_TSS_TASK_B0, 0}
						   , (sched_entry){GDT_TSS_TASK_B1, 0}
						   , (sched_entry){GDT_TSS_TASK_B2, 0}
						   , (sched_entry){GDT_TSS_TASK_B3, 0}
						   , (sched_entry){GDT_TSS_TASK_B4, 0}
						   , (sched_entry){GDT_TSS_TASK_B5, 0}
						   , (sched_entry){GDT_TSS_TASK_B6, 0}
						   , (sched_entry){GDT_TSS_TASK_B7, 0}
						   , (sched_entry){GDT_TSS_TASK_B8, 0}
						   , (sched_entry){GDT_TSS_TASK_B9, 0}};

//sched_entry* tareasAB[2] = {tareasA, tareasB};

void sched_init() {
  //inicializar tss tareas
  for(uint32_t i = 0; i < 10; i++){
    tasksA_cr3[i] = tss_inicializar_tarea(0x10000, TASK_A_SHARED, tareasA[i].indice_gdt);
    tasksB_cr3[i] = tss_inicializar_tarea(0x12000, TASK_B_SHARED, tareasB[i].indice_gdt);
  }
}

int16_t sched_nextTask() {
  current_player = (current_player + 1) % 2;
  sched_entry *tareas = current_player == 0 ? tareasA : tareasB;
  unsigned int prox_tarea = (tareaActualAB[current_player] + 1) % 10;

  while(!tareas[prox_tarea].activo){
    if (prox_tarea == tareaActualAB[current_player]) {
      return GDT_TSS_IDLE << 3;
    } // Cicle
    prox_tarea = (prox_tarea + 1) % 10;
  }
  tareaActualAB[current_player] = prox_tarea;
  return (((uint16_t)tareas[prox_tarea].indice_gdt) << 3) | 3;

}

void sched_deactivateCurrentTask() {
  uint8_t cp = current_player;
  uint8_t ct = tareaActualAB[cp];
  sched_entry* tareas = cp == 0 ? tareasA : tareasB;
  uint32_t shared = cp == 0 ? TASK_A_SHARED : TASK_A_SHARED;
  /*TODO: para algún futuro, estaría bueno recordar las páginas de tss y re utilizarlas, esto permite spawnear infinitas tareas*/
  uint32_t *tasks_cr3 = cp == 0 ? tasksA_cr3 : tasksB_cr3;
  tasks_cr3[ct] = tss_inicializar_tarea(0x10000 * cp*PAGE_SIZE, shared, tareas[ct].indice_gdt);
  game_killMiner(cp, ct);
}
