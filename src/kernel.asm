;; ** por compatibilidad se omiten tildes **
;; ==============================================================================
;; TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
;; ==============================================================================

%include "print.mac"
%include "colors.mac"

extern GDT_TSS_INITIAL
extern GDT_DESC
extern IDT_DESC
extern KERNEL_PAGE_DIR
extern idt_init
extern mmu_init
extern mmu_initKernelDir
extern mmu_initTaskDir_Pos
extern mmu_initTaskDir
extern pic_enable
extern pic_reset
extern tss_init
extern tss_inicializar_idle
extern sched_init

extern game_init
extern game_screen_drawGameBoard

global start

;; Saltear seccion de datos
    jmp start

;;
;; Seccion de datos.
;; -------------------------------------------------------------------------- ;;
    start_rm_msg db     'Iniciando kernel en Modo Real'
    start_rm_len equ    $ - start_rm_msg

    start_pm_msg db     'Iniciando kernel en Modo Protegido'
    start_pm_len equ    $ - start_pm_msg

;;
;; Seccion de tablero
;; -------------------------------------------------------------------------- ;;
    board_char times 78 db 0
    board_char_len equ $ - board_char

;;
;; Seccion de código.
;; -------------------------------------------------------------------------- ;;

;; Punto de entrada del kernel.
BITS 16
start:
    ; Deshabilitar interrupciones
    cli

    ; Cambiar modo de video a 80 X 50
    mov ax, 0003h
    int 10h ; set mode 03h
    xor bx, bx
    mov ax, 1112h
    int 10h ; load 8x8 font

    ; Imprimir mensaje de bienvenida
    print_text_rm start_rm_msg, start_rm_len, 0x07, 0, 0

    ; Habilitar A20

    ; Cargar la GDT
    lgdt [GDT_DESC]

    ; Setear el bit PE del registro CR0
    mov eax, cr0
    or eax, 1
    mov cr0, eax

    ; Saltar a modo protegido
    jmp 0x90:.modo_protegido	 	; salto largo a indice 18 shifteado 3 bits

.modo_protegido:
BITS 32

    ; Establecer selectores de segmentos
    mov ax, 0x98
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov gs, ax
    mov fs, ax

    ; Establecer la base de la pila
    mov ebp, 0x27000                    ; pegada al kernel, push primero mueve el rsp y luego hace el store
    mov esp, ebp

    ; Imprimir mensaje de bienvenida
    print_text_pm start_pm_msg, start_pm_len, 0x07, 1, 0

    ; Inicializar pantalla

    mov dx, 0xb0
    mov ds, dx              ; selector de segmentos de video
    mov ecx, 80*50
.init_screen:
    mov word [2*ecx - 2], 0x0000
    loop .init_screen
    mov ds, ax              ; recupero el selector de segmentos
.init_board:
    call game_screen_drawGameBoard      ; para ir viendo algo...
    ; Inicializar el manejador de memoria
	call mmu_init
    ; Inicializar el directorio de paginas
	call mmu_initKernelDir

    ; Cargar directorio de paginas
	mov eax, 0x00027000
	mov cr3, eax

    ; Habilitar paginacion
    mov eax, cr0
    or eax, 0x80000000
    mov cr0, eax

    ; Inicializar tss
    call tss_init
    ; Inicializar tss de la tarea Idle

    call tss_inicializar_idle

    ; Inicializar el scheduler
    call sched_init

    ; Inicializar la IDT
    call idt_init
    ; Cargar IDT
    lidt [IDT_DESC]

    ; Configurar controlador de interrupciones
    call pic_reset
    call pic_enable

    ; inicializo el juego y la pantalla
    call game_init

    ; Cargar tarea inicial
    mov ax, 23 << 3
    ltr ax
    ; Habilitar interrupciones
    sti
    ; Saltar a la primera tarea: Idle
    jmp 24<<3:0

    ; Ciclar infinitamente (por si algo sale mal...)
    mov eax, 0xFFFF
    mov ebx, 0xFFFF
    mov ecx, 0xFFFF
    mov edx, 0xFFFF

    jmp $

;; -------------------------------------------------------------------------- ;;

%include "a20.asm"
