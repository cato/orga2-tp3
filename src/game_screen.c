/* ** por compatibilidad se omiten tildes **
   ================================================================================
   TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
   ================================================================================
   rutina/s para debuggear
*/

#include "game.h"
#include "game_screen.h"
#include "screen.h"
#include "chirimbolo.h"

char* greg[8] = {"eax:", "ecx:", "edx:", "ebx:", "esp:", "ebp:", "esi:", "edi:"};

/* lo que vamos a usar con valores por defecto */
dbg_box_sty_t dbs = {
                     .banner.col = C_BG_BLACK | C_BLINK | C_FG_RED,
                     .banner.cha = CH_DWARF,
                     .body.col = C_FG_BROWN,
                     .body.cha = CH_BLOCK,
                     .regs.col = C_FG_GREEN | C_BG_BROWN,
                     .vals.col = C_FG_LIGHT_GREEN | C_BG_BROWN,
                     /* geometry */
                     .ini.x = 22,
                     .ini.y = 1,
};

uint8_t crystals_lvl_sty[3] = {C_FG_LIGHT_GREEN | C_BG_GREEN,
                               C_FG_LIGHT_BROWN | C_BG_BROWN,
                               C_FG_LIGHT_MAGENTA | C_BG_MAGENTA};

screen_sty_t screen_sty = {.board.col = C_FG_BLACK | C_BG_LIGHT_GREY,
                           .board.cha = 0x0,
                           .playerA_corridor = {.cha = 0x0, .col = 0x0},
                           .playerB_corridor = {.cha = 0x0, .col = 0x0}};

void box_set_width(dbg_box_sty_t* dbs, uint8_t w) {
  dbs->end.x = dbs->ini.x + w;
}
void box_set_height(dbg_box_sty_t* dbs, uint8_t h) { dbs->end.y = dbs->ini.y + h; }

uint8_t box_get_width(dbg_box_sty_t* dbs) { return  dbs->end.x - dbs->ini.x; }
uint8_t box_get_height(dbg_box_sty_t* dbs) { return dbs->end.y - dbs->ini.y; }

extern player_t playerA;
extern player_t playerB;
extern uint32_t (*map)[BOARD_SIZE_X];

void game_screen_init() {
  dbs.borders = dbs.banner;
  box_set_width(&dbs, 40);
  dbs.end.y = VIDEO_FILS;

  player_sty_t playerA_sty = {.sty = {.col = C_FG_CYAN, .cha = CH_DWARF}};
  player_sty_t playerB_sty = {.sty = {.col = C_FG_RED, .cha = CH_DWARF}};

  playerA_sty.corridor = &screen_sty.playerA_corridor;
  playerB_sty.corridor = &screen_sty.playerB_corridor;

  playerA.sty = playerA_sty;
  playerB.sty = playerB_sty;

  sty_t minerA_sty = playerA_sty.sty;
  sty_t minerB_sty = playerB_sty.sty;

  for (int i = 0; i < 10; i++) {
      playerA.miners[i].sty = minerA_sty;
      playerB.miners[i].sty = minerB_sty;
  }

  screen_drawBox(playerA.pos.y, playerA.pos.x, 1, 1, playerA_sty.sty.cha, playerA_sty.sty.col);
  screen_drawBox(playerB.pos.y, playerB.pos.x, 1, 1, playerB_sty.sty.cha, playerB_sty.sty.col);

  for (uint8_t x = 0; x < BOARD_SIZE_X; x++){
    for (uint8_t y = 0; y < BOARD_SIZE_Y; y++){
      game_screen_crystalPut(x, y);
    }
  }

}

void game_screen_crystalPut(uint8_t x, uint8_t y) {
  uint32_t crystals = map[x][y];
  if (crystals != 0) {
    uint16_t attr = crystals_lvl_sty[0];
    if (crystals >= 2) {
      attr = crystals_lvl_sty[1];
    }
    if (crystals >= 4) {
      attr = crystals_lvl_sty[2];
    }
    print_dec(crystals, 1, x + 1, y + 1, attr);
  }
}

void game_screen_drawGameBoard() {
  char* header_A = "Jugador A: Urist McMiner  ";
  char* header_B = "Jugador B: Bembul McHauler";
  char* header_score = "         scores         ";

  screen_drawBox(1, 1, 40, 78, screen_sty.board.cha, screen_sty.board.col);
  screen_drawBox(44, 1, 4, 39, 0x0, C_FG_WHITE | C_BG_CYAN);
  screen_drawBox(44, 40, 4, 39, 0x0, C_FG_WHITE | C_BG_GREEN);

  print(header_A, 2, 43, C_FG_WHITE | C_BG_CYAN);
  print(header_B, 52, 43, C_FG_WHITE | C_BG_GREEN);
  print(header_score, 28, 43, C_FG_LIGHT_GREY | C_BG_BROWN);

  /* Display jugador A */
  for (uint8_t i = 0; i < 10; ++i) {
    print_dec(i, 1, (i * 3) + 2, 44, C_FG_WHITE | C_BG_BLACK);
    print_dec(00, 2, (i * 3) + 2, 46, C_FG_WHITE | C_BG_BLACK);
  }
  print_dec(000, 3, 34, 44, C_FG_LIGHT_CYAN | C_BG_BLACK);

  /* Display jugador B */
  for (uint8_t i = 0; i < 10; ++i) {
    print_dec(i, 1, (i * 3) + 49, 44, C_FG_WHITE | C_BG_BLACK);
    print_dec(00, 2, (i * 3) + 49, 46, C_FG_WHITE | C_BG_BLACK);
  }
  print_dec(000, 3, 43, 44, C_FG_LIGHT_GREEN | C_BG_BLACK);
}

/* numero de excepción -> string de descripción - tiene
   que tener 20 chars */
char const *exc_str[19] =
  {
   "NPI", "NPI", "NPI", "NPI", "NPI", "NPI", "NPI", "NPI", "NPI", "NPI",
   "NPI", "NPI", "NPI", "General Protection", "NPI", "NPI", "NPI", "NPI", "NPI"
  };

/*
  0   |    EDI     | (-) <- ESP.0 porque estamos en 32b
  1   |    ESI     |
  2   |    EBP     |
  3   |    ESP     |  <- este no va a tener el valor de la rutina
  4   |    EBX     |
  5   |    EDX     |
  6   |    ECX     |
  7   |    EAX     | <- esto es el pushad
  8   | Error Code | <- ESP de nivel 0 luego de la excepción
  9   |    EIP     |
  10  |    CS      |
  11  |   EFLAGS   |      estos los usamos todos
  12  |    ESP     |
  13  |    SS      |  (+)  <- por entrar a la interrupción

  Luego los otros registros de segmento no cambian, el cr3 tampoco (crX != 3 no
  se, pero no creo que cambien)
*/
void game_screen_printDebugBox(uint8_t exc_n, uint32_t *lvl0_stack) {
  /* dibujo el cuadro y el banner */
  screen_drawBox(dbs.ini.y, dbs.ini.x, box_get_height(&dbs),
                 box_get_width(&dbs), dbs.body.cha, dbs.body.col);
  screen_drawBox(dbs.ini.y, dbs.ini.x, 3, box_get_width(&dbs), dbs.banner.cha, dbs.banner.col);

  // TODO: usar exc_n para exception number
  uint8_t ban_col = dbs.ini.x + 3;
  print("Exception ", ban_col, dbs.ini.y + 1, dbs.banner.col & ~C_BLINK);
  print_dec(exc_n, 2, ban_col + 11, dbs.ini.y + 1, dbs.banner.col & ~C_BLINK);
  print(": ", ban_col + 14, dbs.ini.y + 1, dbs.banner.col & ~C_BLINK);
  print(exc_str[exc_n], ban_col + 16, dbs.ini.y + 1, dbs.banner.col & ~C_BLINK);

  uint8_t left_column = dbs.ini.x + 2;
  uint8_t left_column_vals = left_column + 5;
  uint8_t current_row     = dbs.ini.y + 3;
  /* registros generales */
  for (int i = 7; i >= 0; i--) {
    print(greg[i], left_column, current_row += 2, dbs.regs.col);
    int idx = i == 3 ? i : 12; // rsp no se saca del pushad
    print_hex(lvl0_stack[idx], 8, left_column_vals, current_row, dbs.vals.col);
  }

  current_row += 1;
  /* registros de segmentos */
  uint32_t tsk_addr = gdt[rtr()].base_0_15;
  tsk_addr |= (gdt[rtr()].base_23_16 << 15);
  tsk_addr |= (gdt[rtr()].base_31_24 << 23);
  tss *tsk_tss = (tss *)tsk_addr;

  print(" cs:", left_column, current_row += 2, dbs.regs.col);
  print_hex(lvl0_stack[10], 8, left_column_vals, current_row, dbs.vals.col);
  print(" ds:", left_column, current_row += 2, dbs.regs.col);
  print_hex(tsk_tss->ds , 8, left_column_vals, current_row, dbs.vals.col);
  print(" es:", left_column, current_row += 2, dbs.regs.col);
  print_hex(tsk_tss->es, 8, left_column_vals, current_row, dbs.vals.col);
  print(" fs:", left_column, current_row += 2, dbs.regs.col);
  print_hex(tsk_tss->fs, 8, left_column_vals, current_row, dbs.vals.col);
  print(" gs:", left_column, current_row += 2, dbs.regs.col);
  print_hex(tsk_tss->gs, 8, left_column_vals, current_row, dbs.vals.col);
  print(" ss:", left_column, current_row += 2, dbs.regs.col);
  print_hex(lvl0_stack[13], 8, left_column_vals, current_row, dbs.vals.col);

  /* control regs */
  print("cr0:", left_column, current_row += 3, dbs.regs.col);
  print_hex(rcr0(), 8, left_column_vals, current_row, dbs.vals.col);
  print("cr2:", left_column, current_row += 2, dbs.regs.col);
  print_hex(rcr2(), 8, left_column_vals, current_row, dbs.vals.col);
  print("cr3:", left_column, current_row += 2, dbs.regs.col);
  print_hex(rcr3(), 8, left_column_vals, current_row, dbs.vals.col);
  print("cr4:", left_column, current_row += 2, dbs.regs.col);
  print_hex(rcr4(), 8, left_column_vals, current_row, dbs.vals.col);

  uint8_t last_row = current_row;

  uint8_t middle_column = dbs.ini.x + box_get_width(&dbs) / 2;
  /* task stack */
  current_row = dbs.ini.y + 5;
  uint8_t current_column = middle_column;
  uint32_t *task_stack_pt = (uint32_t *)lvl0_stack[12];

  print("esp:", current_column, current_row, dbs.regs.col);

  current_row++;
  uint8_t size = last_row - current_row;
  for (int j = 0; j < 2; j++) {
    for (int i = 0; i < size; i++) {
      uint8_t idx = i + j*(size + 1);
      print_hex(task_stack_pt[idx], 8, current_column, current_row + i + 1,
                (C_BLINK * ((i + j*size) % 4 == 0)) | dbs.vals.col);
    }
    current_column += 10;
  }
  current_column -= 10;
  print("esp[", current_column, current_row - 1, dbs.regs.col);
  print_hex(size, 2, current_column + 4, current_row - 1, dbs.regs.col);
  print("]:", current_column + 6, current_row - 1, dbs.regs.col);

  /* eflags */
  print("eflags:", middle_column - 8, dbs.end.y - 3, dbs.regs.col);
  print_hex(lvl0_stack[11], 8, middle_column, dbs.end.y - 3, dbs.vals.col);
}

void game_screen_playerRefresh(player_sty_t player_sty, pos_t new_pos, pos_t old_pos) {
  game_screen_playerPut(player_sty.sty, new_pos);
  screen_drawBox(old_pos.y, old_pos.x, 1, 1, player_sty.corridor->cha, player_sty.corridor->col);
}

void game_screen_playerPut(sty_t player_sty, pos_t pos) {
  screen_drawBox(pos.y, pos.x, 1, 1, player_sty.cha,  player_sty.col);
}

void game_screen_minerRefresh(sty_t miner_sty, pos_t new_pos, pos_t old_pos) {
  game_screen_minerPut(miner_sty, new_pos);
  screen_clearFG(old_pos.x+1, old_pos.y+1);
}

void game_screen_minerPut(sty_t miner_sty, pos_t pos) {
  screen_putCharWithFG(miner_sty.cha, miner_sty.col, pos.x+1, pos.y+1);
}

/* otras */

uint8_t game_screen_echo_cursor = 0;
uint8_t game_screen_echo_right_margin = 30;

void game_screen_echo(char *msg) {
  print(msg, game_screen_echo_cursor, VIDEO_FILS-1, 0x0F);
  while (*msg != 0) {
    msg++;
    game_screen_echo_cursor++;
  }
  game_screen_echo_cursor %= game_screen_echo_right_margin;
}

void game_screen_echoDec(uint8_t n) {
  //TODO terminar math_digits y generalizar
  print_dec(n, 8/*math_digits10(n)*/, game_screen_echo_cursor, VIDEO_FILS-1, 0x0F);
  game_screen_echo_cursor += 8;/*math_digits10(n);*/
  game_screen_echo_cursor %= game_screen_echo_right_margin;
}

void game_screen_echoClean() {
  game_screen_echo_cursor = 0;
  screen_drawBox(VIDEO_FILS-1, 0, 1,game_screen_echo_right_margin, 0x0, 0x0);
}
