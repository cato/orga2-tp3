/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================

    Definiciones globales del sistema.
*/

#ifndef __DEFINES_H__
#define __DEFINES_H__

/* Bool */
/* -------------------------------------------------------------------------- */
#define TRUE                    0x00000001
#define FALSE                   0x00000000
#define ERROR                   1

/* Atributos paginas */
/* -------------------------------------------------------------------------- */
#define PAG_P                   0x00000001
#define PAG_R                   0x00000000
#define PAG_RW                  0x00000002
#define PAG_S                   0x00000000
#define PAG_US                  0x00000004

/* Misc */
/* -------------------------------------------------------------------------- */
#define BOARD_SIZE_Y                  40 // Y
#define BOARD_SIZE_X                  78 // X

/* Indices en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_COUNT 64

#define GDT_IDX_NULL_DESC           0
/* Las primeras 17 posiciones se consideran utilizadas */

#define GDT_IDX_CODE_LVL_0          18

#define GDT_IDX_DATA_LVL_0          19

#define GDT_IDX_CODE_LVL_3          20

#define GDT_IDX_DATA_LVL_3          21

#define GDT_IDX_VIDEO_LVL_0         22

/* Indices de TSS */
#define GDT_TSS_INITIAL					23

#define GDT_TSS_IDLE   					24

#define GDT_TSS_FIRST_TASK				25

#define GDT_TSS_TASK_A0					25

#define GDT_TSS_TASK_A1					26

#define GDT_TSS_TASK_A2					27

#define GDT_TSS_TASK_A3					28

#define GDT_TSS_TASK_A4					29

#define GDT_TSS_TASK_A5					30

#define GDT_TSS_TASK_A6					31

#define GDT_TSS_TASK_A7					32

#define GDT_TSS_TASK_A8					33

#define GDT_TSS_TASK_A9					34

#define GDT_TSS_TASK_B0					35

#define GDT_TSS_TASK_B1					36

#define GDT_TSS_TASK_B2					37

#define GDT_TSS_TASK_B3					38

#define GDT_TSS_TASK_B4					39

#define GDT_TSS_TASK_B5					40

#define GDT_TSS_TASK_B6					41

#define GDT_TSS_TASK_B7					42

#define GDT_TSS_TASK_B8					43

#define GDT_TSS_TASK_B9					44
/* Offsets en la gdt */
/* -------------------------------------------------------------------------- */
#define GDT_OFF_NULL_DESC           (GDT_IDX_NULL_DESC << 3)

/* Selectores de segmentos */
/* -------------------------------------------------------------------------- */

/* Direcciones de memoria */
/* -------------------------------------------------------------------------- */
#define BOOTSECTOR               0x00001000 /* direccion fisica de comienzo del bootsector (copiado) */
#define KERNEL                   0x00001200 /* direccion fisica de comienzo del kernel */
#define VIDEO                    0x000B8000 /* direccion fisica del buffer de video */

/* Direcciones virtuales de código, pila y datos */
/* -------------------------------------------------------------------------- */
#define TASK_CODE             0x08000000 /* direccion virtual del codigo */

/* Direcciones fisicas de codigos */
/* -------------------------------------------------------------------------- */
/* En estas direcciones estan los códigos de todas las tareas. De aqui se
 * copiaran al destino indicado por TASK_<i>_CODE_ADDR.
 */

#define TASK_A_CODE 0x10000
#define TASK_B_CODE 0x12000

#define TASK_A_SHARED 0x15000
#define TASK_B_SHARED 0x17000

/* Direcciones fisicas de directorios y tablas de paginas del KERNEL */
/* -------------------------------------------------------------------------- */
#define KERNEL_PAGE_DIR          0x00027000
#define KERNEL_PAGE_TABLE_0      0x00028000
#define CRYSTALS_MAP             0x00029000

#endif  /* !__DEFINES_H__ */
