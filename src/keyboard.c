#include "keyboard.h"
#include "colors.h"
#include "screen.h"

#include "game.h"
#include "game_screen.h"

extern uint8_t debug_mode_on;
extern player_t playerA;
extern player_t playerB;

void keyboard(uint8_t scancode) {
  if (scancode == KB_Y_KEY_DOWN) {
    /* Toggle modo debug.
     * Durante pantalla de debug no se atienden interrupciones.
     */
    debug_mode_on = !debug_mode_on;
    uint8_t color_cartel_debug = debug_mode_on != 0 ? 0x0F : 0x00;
    print("MODO DEBUG", 50, 49, color_cartel_debug);
    return;
  }

  if (scancode >= KB_NUMPAD_RELEASED_BEG && scancode < KB_NUMPAD_RELEASED_END) {
    scancode -= KB_NUMPAD_RELEASED_BEG;
    scancode += 1;
    scancode %= 10;
    print_dec(scancode, 1, VIDEO_COLS - 1, 0, C_FG_WHITE | C_BG_BLACK);
    return;
  }

  switch(scancode){
  case KB_W_KEY_DOWN:
    game_movePlayer(&playerA, Left);
    return;
  case KB_S_KEY_DOWN:
    game_movePlayer(&playerA, Right);
    return;
  case KB_I_KEY_DOWN:
    game_movePlayer(&playerB, Right);
    return;
  case KB_K_KEY_DOWN:
    game_movePlayer(&playerB, Left);
    return;
  }

   switch (scancode) {
   case KB_L_SHIFT_DOWN:
     game_newTask(&playerA, Right);
     break;
   case KB_R_SHIFT_DOWN:
     game_newTask(&playerB, Left);
   }
}
