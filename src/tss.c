/* ** por compatibilidad se omiten tildes **
================================================================================
 TRABAJO PRACTICO 3 - System Programming - ORGANIZACION DE COMPUTADOR II - FCEN
================================================================================
  definicion de estructuras para administrar tareas
*/

#include "tss.h"

tss tss_initial;
tss tss_idle;

void tss_init() {
	tss_inicializar_gdt_entry(GDT_TSS_INITIAL, (uint32_t) &tss_initial, 1);
}


void tss_inicializar_gdt_entry(uint32_t index, uint32_t base, uint8_t is_kernel){
    gdt[index].limit_0_15   = 0x0067;  				//104 bytes que es el tamaño de una TSS
    gdt[index].base_0_15    = base & 0xFFFF; 		//Me quedo con los primeros 15 bits de la base.
    gdt[index].base_23_16   = (base >> 16) & 0xFF; 	//Shifteo y me quedo con los últimos 8 bits de la base.
    gdt[index].type         = 0x9; 					//Tipo de segmento (tiene que valer 10B1, con B de busy, así que vale 1001 = 9.
    gdt[index].s            = 0x0; 					//Vale 0 porque así lo dicen las diapos.
    gdt[index].dpl          = (is_kernel?0x0:0x3);  //Es del Kernel o del usuario?
    gdt[index].p            = 0x1; 					//Present
    gdt[index].limit_16_19  = 0x0; 					//Límite
    gdt[index].avl          = 0x0; 					//Disponible para ser usado por software
    gdt[index].l            = 0x0; 					//Vale 0 porque así lo dicen las diapos.
    gdt[index].db           = 0x0; 					//Vale 0 porque así lo dicen las diapos.
    gdt[index].g            = 0x0; 					//Granularidad
	gdt[index].base_31_24   = (base >> 24);
}

void tss_inicializar_idle() {
	//Inicializo la TSS de la tarea Idle
	tss_idle.ptl 		= 0x0000;
    tss_idle.unused0 	= 0x0000;
    tss_idle.esp0 		= 0x00026FFF; 	//Ubicación de la base del stack del kernel
    tss_idle.ss0 		= GDT_IDX_CODE_LVL_0 << 3; 	//selector de segmento para la entrada del codigo del kernel de la gdt
    tss_idle.unused1 	= 0x0000;
    tss_idle.esp1 		= 0x00000000;
    tss_idle.ss1 		= 0x0000;
    tss_idle.unused2 	= 0x0000;
    tss_idle.esp2 		= 0x00000000;
    tss_idle.ss2 		= 0x0000;
    tss_idle.unused3 	= 0x0000;
    tss_idle.cr3 		= KERNEL_PAGE_DIR; 	//Direccion del page directory del kernel
    tss_idle.eip 		= 0x00014000; 	//Dirección de la tarea idle
    tss_idle.eflags 	= 0x00000202;
    tss_idle.eax 		= 0x00000000;
    tss_idle.ecx 		= 0x00000000;
    tss_idle.edx 		= 0x00000000;
    tss_idle.ebx 		= 0x00000000;
    tss_idle.esp 		= 0x00026FFF; 	//Ubicación del tope del stack del kernel
    tss_idle.ebp 		= 0x00026FFF; 	//Ubicación de la base del stack del kernel
    tss_idle.esi 		= 0x00000000;
    tss_idle.edi 		= 0x00000000;
    tss_idle.es 		= GDT_IDX_DATA_LVL_0 << 3; 	//selector de segmento para la entrada de datos del kernel de la gdt (ya
                                        //esta mapeada por la funcion mapear_dir_kernel, que mapea todas las paginas
                                        //que entran en una entrada del PD del kernel, por lo que el stack queda adentro)
    tss_idle.unused4 	= 0x0000;
    tss_idle.cs 		= GDT_IDX_CODE_LVL_0 << 3; 	//selector de segmento para la entrada de codigo del kernel de la gdt
    tss_idle.unused5 	= 0x0000;
    tss_idle.ss 		= GDT_IDX_DATA_LVL_0 << 3;
    tss_idle.unused6 	= 0x0000;
    tss_idle.ds 		= GDT_IDX_DATA_LVL_0 << 3;
    tss_idle.unused7 	= 0x0000;
    tss_idle.fs 		= GDT_IDX_DATA_LVL_0 << 3;
    tss_idle.unused8 	= 0x0000;
    tss_idle.gs 		= GDT_IDX_DATA_LVL_0 << 3;
    tss_idle.unused9 	= 0x0000;
    tss_idle.ldt 		= 0x0000;
    tss_idle.unused10 	= 0x0000;
    tss_idle.dtrap 		= 0x0000;
	tss_idle.iomap 		= 0xFFFF;

    //Pongo el descriptor de la TSS de la tarea idle en la GDT
    tss_inicializar_gdt_entry(GDT_TSS_IDLE, (uint32_t) &tss_idle, 1); //GDT[0x21]
}

uint32_t tss_inicializar_tarea(uint32_t direccion_tarea, uint32_t direccion_shared, uint32_t gdt_index){
    //Pido una página del área libre de kernel donde guardar la TSS
    tss * direccion_tss = (tss *) mmu_nextFreeKernelPage();

    //Le pido al kernel una página del área libre de tareas para el código de esta tarea
    uint32_t direccion_codigo = mmu_nextFreeKernelPage();

    // Le pido al kernel una página de su área libre para el stack de nivel 0
    // de esta tarea
    uint32_t direccion_stack_top_0 = mmu_nextFreeKernelPage();

    //Pongo en base la dirección de la tss
    uint32_t base = (uint32_t) direccion_tss;

    //Creo un page directory para la tarea
    uint32_t tarea_pagedir = mmu_initTaskDir(TASK_CODE, direccion_codigo, direccion_shared, direccion_tarea);

    //Pongo el descriptor de la TSS de la tarea en la GDT
    tss_inicializar_gdt_entry(gdt_index, base, 0);//Cada vez que llamo esta función me pone la tarea en la dirección siguiente de la GDT.

    //Inicializo la TSS de la tarea
	(*direccion_tss).ptl 		= 0x0000;
    (*direccion_tss).unused0 	= 0x0000;
    (*direccion_tss).esp0 	    = direccion_stack_top_0 + PAGE_SIZE;
    (*direccion_tss).ss0 		= GDT_IDX_DATA_LVL_0 << 3; //Selector de datos de nivel 0, en donde se encuentra el stack.
    (*direccion_tss).unused1 	= 0x0000;
    (*direccion_tss).esp1 	    = 0x00000000;
    (*direccion_tss).ss1 		= 0x0000;
    (*direccion_tss).unused2 	= 0x0000;
    (*direccion_tss).esp2 	    = 0x00000000;
    (*direccion_tss).ss2 		= 0x0000;
    (*direccion_tss).unused3 	= 0x0000;
    (*direccion_tss).cr3 		= tarea_pagedir; 	//Direccion del page directory de la tarea
    (*direccion_tss).eip 		= TASK_CODE; //Dirección VIRTUAL inicial del código de la tarea
    (*direccion_tss).eflags 	= 0x0202; //EFLAGS en 0x202 para admitir interrupciones
    (*direccion_tss).eax 		= 0x00000000; //EAX
    (*direccion_tss).ecx 		= 0x00000000; //ECX
    (*direccion_tss).edx 		= 0x00000000; //EDX
    (*direccion_tss).ebx 		= 0x00000000; //EBX
    (*direccion_tss).esp 		= TASK_CODE + 2*PAGE_SIZE; //Ubicación del tope del stack (al final de la página)
    (*direccion_tss).ebp 		= TASK_CODE + 2*PAGE_SIZE; //Ubicación de la base del stack (al final de la página)
    (*direccion_tss).esi 		= 0x00000000; //ESI
    (*direccion_tss).edi 		= 0x00000000; //EDI
    (*direccion_tss).es 		= (GDT_IDX_DATA_LVL_3 << 3) | 0x3; //selector de segmento para la entrada de datos de la tarea de la gdt (el & 3 es para la dpl del selector).
    (*direccion_tss).unused4 	= 0x0000;
    (*direccion_tss).cs 		= (GDT_IDX_CODE_LVL_3 << 3) | 0x3; //selector de segmento para la entrada de codigo de la tarea de la gdt (el & 3 es para la dpl del selector).
    (*direccion_tss).unused5 	= 0x0000;
    (*direccion_tss).ss 		= (GDT_IDX_DATA_LVL_3 << 3) | 0x3; //SS = ES
    (*direccion_tss).unused6 	= 0x0000;
    (*direccion_tss).ds 		= (GDT_IDX_DATA_LVL_3 << 3) | 0x3; //DS = ES
    (*direccion_tss).unused7 	= 0x0000;
    (*direccion_tss).fs 		= (GDT_IDX_DATA_LVL_3 << 3) | 0x3; //FS = ES
    (*direccion_tss).unused8 	= 0x0000;
    (*direccion_tss).gs 		= (GDT_IDX_DATA_LVL_3 << 3) | 0x3; //GS = ES
    (*direccion_tss).unused9 	= 0x0000;
    (*direccion_tss).ldt 		= 0x0000;
    (*direccion_tss).unused10   = 0x0000;
    (*direccion_tss).dtrap 	    = 0x0000;
	(*direccion_tss).iomap 	    = 0xFFFF;

    return tarea_pagedir;
}
